import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int? count = 0;
  int? i = 01;

  addList() {
    count = count! + 1;
    numList.add(count!);
    setState(() {
      count = count! + 1;
      numList.add(count!);
    });
  }

  List<int> numList = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text("Dynamic list view"),
      ),
      body: Center(
        child :  
          Column(
          children:[ 
            
            Container(
              child: ListView.builder(
                
                itemCount: numList.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.all(10),
                    color: Colors.red,
                    width: 200,
                    height: 100,
                    child: Text(
                      "${numList[index]}",
                      textAlign: TextAlign.center,
                    ),
                  );
                }),
            ),
        ]),
        
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addList,
        child: const Icon(Icons.add),
      ),
    ));
  }
}
